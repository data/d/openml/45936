# OpenML dataset: IndoorScenes

https://www.openml.org/d/45936

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Indoor scene recognition is a challenging open problem in high level vision. Most scene recognition models that work well for outdoor scenes perform poorly in the indoor domain. The main difficulty is that while some indoor scenes (e.g. corridors) can be well characterized by global spatial properties, others (e.g., bookstores) are better characterized by the objects they contain. More generally, to address the indoor scenes recognition problem we need a model that can exploit local and global discriminative information. The database contains 67 Indoor categories, and a total of 15620 images. The number of images varies across categories, but there are at least 100 images per category. All images are in jpg format. The images provided here are for research purposes only.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45936) of an [OpenML dataset](https://www.openml.org/d/45936). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45936/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45936/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45936/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

